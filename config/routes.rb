Rails.application.routes.draw do
  resources :transfers
  resources :regular_entries
  resources :credit_card_entries
  resources :recurrent_entries
  resources :recurrent_entry_infos
  resources :regular_entry_infos
  resources :credit_cards
  resources :containers
  resources :transitions
  resources :base_containers
  resources :categories
  resources :entries
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
