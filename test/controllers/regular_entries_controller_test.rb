require 'test_helper'

class RegularEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @regular_entry = regular_entries(:one)
  end

  test "should get index" do
    get regular_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_regular_entry_url
    assert_response :success
  end

  test "should create regular_entry" do
    assert_difference('RegularEntry.count') do
      post regular_entries_url, params: { regular_entry: { regular_entry_info_id: @regular_entry.regular_entry_info_id } }
    end

    assert_redirected_to regular_entry_url(RegularEntry.last)
  end

  test "should show regular_entry" do
    get regular_entry_url(@regular_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_regular_entry_url(@regular_entry)
    assert_response :success
  end

  test "should update regular_entry" do
    patch regular_entry_url(@regular_entry), params: { regular_entry: { regular_entry_info_id: @regular_entry.regular_entry_info_id } }
    assert_redirected_to regular_entry_url(@regular_entry)
  end

  test "should destroy regular_entry" do
    assert_difference('RegularEntry.count', -1) do
      delete regular_entry_url(@regular_entry)
    end

    assert_redirected_to regular_entries_url
  end
end
