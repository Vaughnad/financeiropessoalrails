require 'test_helper'

class CreditCardEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @credit_card_entry = credit_card_entries(:one)
  end

  test "should get index" do
    get credit_card_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_credit_card_entry_url
    assert_response :success
  end

  test "should create credit_card_entry" do
    assert_difference('CreditCardEntry.count') do
      post credit_card_entries_url, params: { credit_card_entry: { credit_card_id: @credit_card_entry.credit_card_id, expiration_date: @credit_card_entry.expiration_date } }
    end

    assert_redirected_to credit_card_entry_url(CreditCardEntry.last)
  end

  test "should show credit_card_entry" do
    get credit_card_entry_url(@credit_card_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_credit_card_entry_url(@credit_card_entry)
    assert_response :success
  end

  test "should update credit_card_entry" do
    patch credit_card_entry_url(@credit_card_entry), params: { credit_card_entry: { credit_card_id: @credit_card_entry.credit_card_id, expiration_date: @credit_card_entry.expiration_date } }
    assert_redirected_to credit_card_entry_url(@credit_card_entry)
  end

  test "should destroy credit_card_entry" do
    assert_difference('CreditCardEntry.count', -1) do
      delete credit_card_entry_url(@credit_card_entry)
    end

    assert_redirected_to credit_card_entries_url
  end
end
