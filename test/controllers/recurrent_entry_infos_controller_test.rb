require 'test_helper'

class RecurrentEntryInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recurrent_entry_info = recurrent_entry_infos(:one)
  end

  test "should get index" do
    get recurrent_entry_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_recurrent_entry_info_url
    assert_response :success
  end

  test "should create recurrent_entry_info" do
    assert_difference('RecurrentEntryInfo.count') do
      post recurrent_entry_infos_url, params: { recurrent_entry_info: { active: @recurrent_entry_info.active, auto_adjust: @recurrent_entry_info.auto_adjust, auto_confirm: @recurrent_entry_info.auto_confirm, category_id: @recurrent_entry_info.category_id, container_id: @recurrent_entry_info.container_id, estimated_value: @recurrent_entry_info.estimated_value, expiration_date: @recurrent_entry_info.expiration_date, expiration_day: @recurrent_entry_info.expiration_day, name: @recurrent_entry_info.name, nature: @recurrent_entry_info.nature, quotas: @recurrent_entry_info.quotas, user_id: @recurrent_entry_info.user_id } }
    end

    assert_redirected_to recurrent_entry_info_url(RecurrentEntryInfo.last)
  end

  test "should show recurrent_entry_info" do
    get recurrent_entry_info_url(@recurrent_entry_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_recurrent_entry_info_url(@recurrent_entry_info)
    assert_response :success
  end

  test "should update recurrent_entry_info" do
    patch recurrent_entry_info_url(@recurrent_entry_info), params: { recurrent_entry_info: { active: @recurrent_entry_info.active, auto_adjust: @recurrent_entry_info.auto_adjust, auto_confirm: @recurrent_entry_info.auto_confirm, category_id: @recurrent_entry_info.category_id, container_id: @recurrent_entry_info.container_id, estimated_value: @recurrent_entry_info.estimated_value, expiration_date: @recurrent_entry_info.expiration_date, expiration_day: @recurrent_entry_info.expiration_day, name: @recurrent_entry_info.name, nature: @recurrent_entry_info.nature, quotas: @recurrent_entry_info.quotas, user_id: @recurrent_entry_info.user_id } }
    assert_redirected_to recurrent_entry_info_url(@recurrent_entry_info)
  end

  test "should destroy recurrent_entry_info" do
    assert_difference('RecurrentEntryInfo.count', -1) do
      delete recurrent_entry_info_url(@recurrent_entry_info)
    end

    assert_redirected_to recurrent_entry_infos_url
  end
end
