require 'test_helper'

class BaseContainersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @base_container = base_containers(:one)
  end

  test "should get index" do
    get base_containers_url
    assert_response :success
  end

  test "should get new" do
    get new_base_container_url
    assert_response :success
  end

  test "should create base_container" do
    assert_difference('BaseContainer.count') do
      post base_containers_url, params: { base_container: { active: @base_container.active, name: @base_container.name, pay_debit: @base_container.pay_debit, type: @base_container.type, user_id: @base_container.user_id } }
    end

    assert_redirected_to base_container_url(BaseContainer.last)
  end

  test "should show base_container" do
    get base_container_url(@base_container)
    assert_response :success
  end

  test "should get edit" do
    get edit_base_container_url(@base_container)
    assert_response :success
  end

  test "should update base_container" do
    patch base_container_url(@base_container), params: { base_container: { active: @base_container.active, name: @base_container.name, pay_debit: @base_container.pay_debit, type: @base_container.type, user_id: @base_container.user_id } }
    assert_redirected_to base_container_url(@base_container)
  end

  test "should destroy base_container" do
    assert_difference('BaseContainer.count', -1) do
      delete base_container_url(@base_container)
    end

    assert_redirected_to base_containers_url
  end
end
