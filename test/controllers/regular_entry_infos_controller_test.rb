require 'test_helper'

class RegularEntryInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @regular_entry_info = regular_entry_infos(:one)
  end

  test "should get index" do
    get regular_entry_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_regular_entry_info_url
    assert_response :success
  end

  test "should create regular_entry_info" do
    assert_difference('RegularEntryInfo.count') do
      post regular_entry_infos_url, params: { regular_entry_info: { auto_adjust: @regular_entry_info.auto_adjust, category_id: @regular_entry_info.category_id, estimated_value: @regular_entry_info.estimated_value, name: @regular_entry_info.name, nature: @regular_entry_info.nature, user_id: @regular_entry_info.user_id } }
    end

    assert_redirected_to regular_entry_info_url(RegularEntryInfo.last)
  end

  test "should show regular_entry_info" do
    get regular_entry_info_url(@regular_entry_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_regular_entry_info_url(@regular_entry_info)
    assert_response :success
  end

  test "should update regular_entry_info" do
    patch regular_entry_info_url(@regular_entry_info), params: { regular_entry_info: { auto_adjust: @regular_entry_info.auto_adjust, category_id: @regular_entry_info.category_id, estimated_value: @regular_entry_info.estimated_value, name: @regular_entry_info.name, nature: @regular_entry_info.nature, user_id: @regular_entry_info.user_id } }
    assert_redirected_to regular_entry_info_url(@regular_entry_info)
  end

  test "should destroy regular_entry_info" do
    assert_difference('RegularEntryInfo.count', -1) do
      delete regular_entry_info_url(@regular_entry_info)
    end

    assert_redirected_to regular_entry_infos_url
  end
end
