class CreateRegularEntryInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :regular_entry_infos do |t|
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true
      t.string :nature
      t.string :name
      t.decimal :estimated_value, precision: 10, scale: 2
      t.boolean :auto_adjust

      t.timestamps
    end
  end
end
