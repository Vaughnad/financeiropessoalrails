class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :nature
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
