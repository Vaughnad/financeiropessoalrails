class CreateRecurrentEntryInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :recurrent_entry_infos do |t|
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true
      t.string :nature
      t.string :name
      t.decimal :estimated_value, precision: 10, scale: 2
      t.integer :expiration_day
      t.date :expiration_date
      t.integer :quotas
      t.boolean :active
      t.boolean :auto_confirm
      t.references :container, foreign_key: { to_table: :base_containers }
      t.boolean :auto_adjust

      t.timestamps
    end
  end
end
