class CreateBaseContainers < ActiveRecord::Migration[5.0]
  def change
    create_table :base_containers do |t|
      t.references :user, foreign_key: true
      t.string :type
      t.string :name
      t.boolean :pay_debit
      t.boolean :active
      t.decimal :starting_balance, precision: 10, scale: 2
      t.integer :expiration_day

      t.timestamps
    end
  end
end
