class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.references :user, foreign_key: true
      t.string :type
      t.references :category, foreign_key: true
      t.string :name
      t.string :nature
      t.references :regular_entry_info, foreign_key: true
      t.references :recurrent_entry_info, foreign_key: true
      t.references :credit_card, foreign_key: { to_table: :base_containers }
      t.date :expiration_date
      t.integer :quota


      t.timestamps
    end
  end
end
