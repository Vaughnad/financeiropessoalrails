class CreateTransfers < ActiveRecord::Migration[5.0]
  def change
    create_table :transfers do |t|
      t.references :user, foreign_key: true
      t.references :from_container, foreign_key: { to_table: :base_containers }
      t.references :to_container, foreign_key: { to_table: :base_containers }
      t.date :transfer_date
      t.decimal :value, precision: 10, scale: 2

      t.timestamps
    end
  end
end
