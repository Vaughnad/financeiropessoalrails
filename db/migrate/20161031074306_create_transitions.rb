class CreateTransitions < ActiveRecord::Migration[5.0]
  def change
    create_table :transitions do |t|
      t.references :entry, foreign_key: true
      t.decimal :value, precision: 10, scale: 2
      t.date :date
      t.string :payment_option
      t.references :base_container, foreign_key: true
      t.integer :divided
      t.boolean :billing_card_closed
      t.boolean :canceled
      t.boolean :confirmed

      t.timestamps
    end
  end
end
