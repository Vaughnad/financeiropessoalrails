# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161031110107) do

  create_table "base_containers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "type"
    t.string   "name"
    t.boolean  "pay_debit"
    t.boolean  "active"
    t.decimal  "starting_balance", precision: 10, scale: 2
    t.integer  "expiration_day"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["user_id"], name: "index_base_containers_on_user_id", using: :btree
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "nature"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_categories_on_category_id", using: :btree
    t.index ["user_id"], name: "index_categories_on_user_id", using: :btree
  end

  create_table "entries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "type"
    t.integer  "category_id"
    t.string   "name"
    t.string   "nature"
    t.integer  "regular_entry_info_id"
    t.integer  "recurrent_entry_info_id"
    t.integer  "credit_card_id"
    t.date     "expiration_date"
    t.integer  "quota"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["category_id"], name: "index_entries_on_category_id", using: :btree
    t.index ["credit_card_id"], name: "index_entries_on_credit_card_id", using: :btree
    t.index ["recurrent_entry_info_id"], name: "index_entries_on_recurrent_entry_info_id", using: :btree
    t.index ["regular_entry_info_id"], name: "index_entries_on_regular_entry_info_id", using: :btree
    t.index ["user_id"], name: "index_entries_on_user_id", using: :btree
  end

  create_table "recurrent_entry_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.string   "nature"
    t.string   "name"
    t.decimal  "estimated_value", precision: 10, scale: 2
    t.integer  "expiration_day"
    t.date     "expiration_date"
    t.integer  "quotas"
    t.boolean  "active"
    t.boolean  "auto_confirm"
    t.integer  "container_id"
    t.boolean  "auto_adjust"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["category_id"], name: "index_recurrent_entry_infos_on_category_id", using: :btree
    t.index ["container_id"], name: "index_recurrent_entry_infos_on_container_id", using: :btree
    t.index ["user_id"], name: "index_recurrent_entry_infos_on_user_id", using: :btree
  end

  create_table "regular_entry_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.string   "nature"
    t.string   "name"
    t.decimal  "estimated_value", precision: 10, scale: 2
    t.boolean  "auto_adjust"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["category_id"], name: "index_regular_entry_infos_on_category_id", using: :btree
    t.index ["user_id"], name: "index_regular_entry_infos_on_user_id", using: :btree
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transfers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "from_container_id"
    t.integer  "to_container_id"
    t.date     "transfer_date"
    t.decimal  "value",             precision: 10, scale: 2
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["from_container_id"], name: "index_transfers_on_from_container_id", using: :btree
    t.index ["to_container_id"], name: "index_transfers_on_to_container_id", using: :btree
    t.index ["user_id"], name: "index_transfers_on_user_id", using: :btree
  end

  create_table "transitions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "entry_id"
    t.decimal  "value",               precision: 10, scale: 2
    t.date     "date"
    t.string   "payment_option"
    t.integer  "base_container_id"
    t.integer  "divided"
    t.boolean  "billing_card_closed"
    t.boolean  "canceled"
    t.boolean  "confirmed"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["base_container_id"], name: "index_transitions_on_base_container_id", using: :btree
    t.index ["entry_id"], name: "index_transitions_on_entry_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
  end

  add_foreign_key "base_containers", "users"
  add_foreign_key "categories", "categories"
  add_foreign_key "categories", "users"
  add_foreign_key "entries", "base_containers", column: "credit_card_id"
  add_foreign_key "entries", "categories"
  add_foreign_key "entries", "recurrent_entry_infos"
  add_foreign_key "entries", "regular_entry_infos"
  add_foreign_key "entries", "users"
  add_foreign_key "recurrent_entry_infos", "base_containers", column: "container_id"
  add_foreign_key "recurrent_entry_infos", "categories"
  add_foreign_key "recurrent_entry_infos", "users"
  add_foreign_key "regular_entry_infos", "categories"
  add_foreign_key "regular_entry_infos", "users"
  add_foreign_key "transfers", "base_containers", column: "from_container_id"
  add_foreign_key "transfers", "base_containers", column: "to_container_id"
  add_foreign_key "transfers", "users"
  add_foreign_key "transitions", "base_containers"
  add_foreign_key "transitions", "entries"
  add_foreign_key "users", "roles"
end
