-- Create the user account and grant it all privileges on the databases.
grant all PRIVILEGES on financeiro_development.* to financeiro@localhost identified by 'financeiro';
grant all PRIVILEGES on financeiro_test.* to financeiro@localhost identified by 'financeiro';
grant all PRIVILEGES on financeiro_stage.* to financeiro@localhost identified by 'financeiro';
grant all PRIVILEGES on financeiro.* to financeiro@localhost identified by 'financeiro';
