class RegularEntryInfosController < ApplicationController
  before_action :set_regular_entry_info, only: [:show, :edit, :update, :destroy]

  # GET /regular_entry_infos
  # GET /regular_entry_infos.json
  def index
    @regular_entry_infos = RegularEntryInfo.all
  end

  # GET /regular_entry_infos/1
  # GET /regular_entry_infos/1.json
  def show
  end

  # GET /regular_entry_infos/new
  def new
    @regular_entry_info = RegularEntryInfo.new
  end

  # GET /regular_entry_infos/1/edit
  def edit
  end

  # POST /regular_entry_infos
  # POST /regular_entry_infos.json
  def create
    @regular_entry_info = RegularEntryInfo.new(regular_entry_info_params)

    respond_to do |format|
      if @regular_entry_info.save
        format.html { redirect_to @regular_entry_info, notice: 'Regular entry info was successfully created.' }
        format.json { render :show, status: :created, location: @regular_entry_info }
      else
        format.html { render :new }
        format.json { render json: @regular_entry_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /regular_entry_infos/1
  # PATCH/PUT /regular_entry_infos/1.json
  def update
    respond_to do |format|
      if @regular_entry_info.update(regular_entry_info_params)
        format.html { redirect_to @regular_entry_info, notice: 'Regular entry info was successfully updated.' }
        format.json { render :show, status: :ok, location: @regular_entry_info }
      else
        format.html { render :edit }
        format.json { render json: @regular_entry_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /regular_entry_infos/1
  # DELETE /regular_entry_infos/1.json
  def destroy
    @regular_entry_info.destroy
    respond_to do |format|
      format.html { redirect_to regular_entry_infos_url, notice: 'Regular entry info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_regular_entry_info
      @regular_entry_info = RegularEntryInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def regular_entry_info_params
      params.require(:regular_entry_info).permit(:user_id, :category_id, :nature, :name, :estimated_value, :auto_adjust)
    end
end
