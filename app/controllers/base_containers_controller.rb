class BaseContainersController < ApplicationController
  before_action :set_base_container, only: [:show, :edit, :update, :destroy]

  # GET /base_containers
  # GET /base_containers.json
  def index
    @base_containers = BaseContainer.all
  end

  # GET /base_containers/1
  # GET /base_containers/1.json
  def show
  end

  # GET /base_containers/new
  def new
    @base_container = BaseContainer.new
  end

  # GET /base_containers/1/edit
  def edit
  end

  # POST /base_containers
  # POST /base_containers.json
  def create
    @base_container = BaseContainer.new(base_container_params)

    respond_to do |format|
      if @base_container.save
        format.html { redirect_to @base_container, notice: 'Base container was successfully created.' }
        format.json { render :show, status: :created, location: @base_container }
      else
        format.html { render :new }
        format.json { render json: @base_container.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_containers/1
  # PATCH/PUT /base_containers/1.json
  def update
    respond_to do |format|
      if @base_container.update(base_container_params)
        format.html { redirect_to @base_container, notice: 'Base container was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_container }
      else
        format.html { render :edit }
        format.json { render json: @base_container.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_containers/1
  # DELETE /base_containers/1.json
  def destroy
    @base_container.destroy
    respond_to do |format|
      format.html { redirect_to base_containers_url, notice: 'Base container was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_container
      @base_container = BaseContainer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_container_params
      params.require(:base_container).permit(:user_id, :type, :name, :pay_debit, :active)
    end
end
