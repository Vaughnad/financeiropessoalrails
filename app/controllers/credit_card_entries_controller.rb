class CreditCardEntriesController < ApplicationController
  before_action :set_credit_card_entry, only: [:show, :edit, :update, :destroy]

  # GET /credit_card_entries
  # GET /credit_card_entries.json
  def index
    @credit_card_entries = CreditCardEntry.all
  end

  # GET /credit_card_entries/1
  # GET /credit_card_entries/1.json
  def show
  end

  # GET /credit_card_entries/new
  def new
    @credit_card_entry = CreditCardEntry.new
  end

  # GET /credit_card_entries/1/edit
  def edit
  end

  # POST /credit_card_entries
  # POST /credit_card_entries.json
  def create
    @credit_card_entry = CreditCardEntry.new(credit_card_entry_params)

    respond_to do |format|
      if @credit_card_entry.save
        format.html { redirect_to @credit_card_entry, notice: 'Credit card entry was successfully created.' }
        format.json { render :show, status: :created, location: @credit_card_entry }
      else
        format.html { render :new }
        format.json { render json: @credit_card_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /credit_card_entries/1
  # PATCH/PUT /credit_card_entries/1.json
  def update
    respond_to do |format|
      if @credit_card_entry.update(credit_card_entry_params)
        format.html { redirect_to @credit_card_entry, notice: 'Credit card entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @credit_card_entry }
      else
        format.html { render :edit }
        format.json { render json: @credit_card_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /credit_card_entries/1
  # DELETE /credit_card_entries/1.json
  def destroy
    @credit_card_entry.destroy
    respond_to do |format|
      format.html { redirect_to credit_card_entries_url, notice: 'Credit card entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_card_entry
      @credit_card_entry = CreditCardEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_card_entry_params
      params.require(:credit_card_entry).permit(:credit_card_id, :expiration_date)
    end
end
