class RegularEntriesController < ApplicationController
  before_action :set_regular_entry, only: [:show, :edit, :update, :destroy]

  # GET /regular_entries
  # GET /regular_entries.json
  def index
    @regular_entries = RegularEntry.all
  end

  # GET /regular_entries/1
  # GET /regular_entries/1.json
  def show
  end

  # GET /regular_entries/new
  def new
    @regular_entry = RegularEntry.new
  end

  # GET /regular_entries/1/edit
  def edit
  end

  # POST /regular_entries
  # POST /regular_entries.json
  def create
    @regular_entry = RegularEntry.new(regular_entry_params)

    respond_to do |format|
      if @regular_entry.save
        format.html { redirect_to @regular_entry, notice: 'Regular entry was successfully created.' }
        format.json { render :show, status: :created, location: @regular_entry }
      else
        format.html { render :new }
        format.json { render json: @regular_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /regular_entries/1
  # PATCH/PUT /regular_entries/1.json
  def update
    respond_to do |format|
      if @regular_entry.update(regular_entry_params)
        format.html { redirect_to @regular_entry, notice: 'Regular entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @regular_entry }
      else
        format.html { render :edit }
        format.json { render json: @regular_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /regular_entries/1
  # DELETE /regular_entries/1.json
  def destroy
    @regular_entry.destroy
    respond_to do |format|
      format.html { redirect_to regular_entries_url, notice: 'Regular entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_regular_entry
      @regular_entry = RegularEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def regular_entry_params
      params.require(:regular_entry).permit(:regular_entry_info_id)
    end
end
