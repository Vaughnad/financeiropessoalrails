class RecurrentEntryInfosController < ApplicationController
  before_action :set_recurrent_entry_info, only: [:show, :edit, :update, :destroy]

  # GET /recurrent_entry_infos
  # GET /recurrent_entry_infos.json
  def index
    @recurrent_entry_infos = RecurrentEntryInfo.all
  end

  # GET /recurrent_entry_infos/1
  # GET /recurrent_entry_infos/1.json
  def show
  end

  # GET /recurrent_entry_infos/new
  def new
    @recurrent_entry_info = RecurrentEntryInfo.new
  end

  # GET /recurrent_entry_infos/1/edit
  def edit
  end

  # POST /recurrent_entry_infos
  # POST /recurrent_entry_infos.json
  def create
    @recurrent_entry_info = RecurrentEntryInfo.new(recurrent_entry_info_params)

    respond_to do |format|
      if @recurrent_entry_info.save
        format.html { redirect_to @recurrent_entry_info, notice: 'Recurrent entry info was successfully created.' }
        format.json { render :show, status: :created, location: @recurrent_entry_info }
      else
        format.html { render :new }
        format.json { render json: @recurrent_entry_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recurrent_entry_infos/1
  # PATCH/PUT /recurrent_entry_infos/1.json
  def update
    respond_to do |format|
      if @recurrent_entry_info.update(recurrent_entry_info_params)
        format.html { redirect_to @recurrent_entry_info, notice: 'Recurrent entry info was successfully updated.' }
        format.json { render :show, status: :ok, location: @recurrent_entry_info }
      else
        format.html { render :edit }
        format.json { render json: @recurrent_entry_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recurrent_entry_infos/1
  # DELETE /recurrent_entry_infos/1.json
  def destroy
    @recurrent_entry_info.destroy
    respond_to do |format|
      format.html { redirect_to recurrent_entry_infos_url, notice: 'Recurrent entry info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recurrent_entry_info
      @recurrent_entry_info = RecurrentEntryInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recurrent_entry_info_params
      params.require(:recurrent_entry_info).permit(:user_id, :category_id, :nature, :name, :estimated_value, :expiration_day, :expiration_date, :quotas, :active, :auto_confirm, :container_id, :auto_adjust)
    end
end
