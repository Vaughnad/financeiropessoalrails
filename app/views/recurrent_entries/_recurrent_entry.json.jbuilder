json.extract! recurrent_entry, :id, :recurrent_entry_info_id, :quota, :expiration_date, :created_at, :updated_at
json.url recurrent_entry_url(recurrent_entry, format: :json)