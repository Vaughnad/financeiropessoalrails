json.extract! base_container, :id, :user_id, :type, :name, :pay_debit, :active, :created_at, :updated_at
json.url base_container_url(base_container, format: :json)