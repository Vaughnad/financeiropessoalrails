json.extract! transfer, :id, :user_id, :from_container_id, :to_container_id, :transfer_date, :value, :created_at, :updated_at
json.url transfer_url(transfer, format: :json)