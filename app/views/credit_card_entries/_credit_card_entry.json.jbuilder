json.extract! credit_card_entry, :id, :credit_card_id, :expiration_date, :created_at, :updated_at
json.url credit_card_entry_url(credit_card_entry, format: :json)