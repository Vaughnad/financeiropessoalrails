json.extract! entry, :id, :user_id, :type, :category_id, :name, :nature, :created_at, :updated_at
json.url entry_url(entry, format: :json)