json.extract! category, :id, :user_id, :name, :nature, :category_id, :created_at, :updated_at
json.url category_url(category, format: :json)