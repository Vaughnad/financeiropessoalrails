json.extract! regular_entry, :id, :regular_entry_info_id, :created_at, :updated_at
json.url regular_entry_url(regular_entry, format: :json)