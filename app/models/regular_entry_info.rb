class RegularEntryInfo < ApplicationRecord
  belongs_to :user
  belongs_to :category
end
