class Transfer < ApplicationRecord
  belongs_to :user
  belongs_to :from_container
  belongs_to :to_container
end
