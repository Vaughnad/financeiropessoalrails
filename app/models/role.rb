class Role < ApplicationRecord
	has_many :users

	TYPE = {
    default: "Default",
    premium: "Premium",
    admin: "Admin"
  }	
end
