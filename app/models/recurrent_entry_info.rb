class RecurrentEntryInfo < ApplicationRecord
  belongs_to :user
  belongs_to :category
  belongs_to :container
end
