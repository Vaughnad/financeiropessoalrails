class Transition < ApplicationRecord
  belongs_to :entry
  belongs_to :base_container
end
